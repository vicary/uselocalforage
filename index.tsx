import LocalForage from "localforage";
import {
  createContext,
  FunctionComponent,
  useContext,
  useEffect,
  useState,
} from "react";

type LocalForageOptions = Parameters<typeof LocalForage["createInstance"]>[0];

const clientCache = new Map<LocalForageOptions, LocalForage>();

const LocalForageContext = createContext<LocalForageOptions>({});

export const LocalForageProvider: FunctionComponent<{
  config?: LocalForageOptions;
}> = ({ config = {}, children }) => (
  <LocalForageContext.Provider value={config}>
    {children}
  </LocalForageContext.Provider>
);

/**
 * Async version of the hook.
 *
 * This hook returns an object similar to `react-use#useAsync` and `@apollo/client#useQuery`.
 */
export function useLocalForageAsync<TState = any>(
  key: string,
  defaultValue?: TState
) {
  const [value, setValue] = useState<TState | undefined>(defaultValue);
  const [loading, setLoading] = useState(false);

  const context = useContext(LocalForageContext);

  if (!clientCache.has(context)) {
    clientCache.set(context, LocalForage.createInstance(context));
  }

  const client = clientCache.get(context)!;

  useEffect(() => {
    setLoading(true);
    client.getItem<TState>(key).then((value) => {
      if (value !== null) {
        setValue(value);
      }

      setLoading(false);
    });
  }, []);

  return { value, setValue, loading, remove: () => client.removeItem(key) };
}

/**
 * Simple version of the hook.
 *
 * This version intentionally mimics the API of `useState`, sacrificing detailed states for ease of use.
 */
export function useLocalForage<TState = any>(
  key: string,
  defaultValue?: TState
) {
  const [value, setValue] = useState<TState | undefined>(defaultValue);

  const context = useContext(LocalForageContext);

  if (!clientCache.has(context)) {
    clientCache.set(context, LocalForage.createInstance(context));
  }

  const client = clientCache.get(context)!;

  useEffect(() => {
    client.getItem<TState>(key).then((value) => {
      if (value !== null) {
        setValue(value);
      }
    });
  }, []);

  return [value, setValue];
}
